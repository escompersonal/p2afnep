package p2afnep;

import java.util.Stack;

/**
 * Automata encagado de validar las cadenas e imprimir los recorrido validos
 *
 * @author carlos
 */
public class Automata {

    private TablaTrans tabla;
    private String inicial;
    private String []aceptacion;

    private Stack<Transicion> recorrido = new Stack<Transicion>(); //pila donde se guardan los recorridos

    private String cadena; //Cadena que se va a validar
    private int cont = 0; //Contadro de recorridos validos

    /**
     * Asigan los elementos que contiene un automata
     *
     * @param tabla : Tabla que contiene las transiciones
     * @param inicial : Estado inicial del automata
     * @param aceptacion : Lista de estado finales
     */
    public Automata(TablaTrans tabla, String inicial, String []aceptacion) {
        this.tabla = tabla;
        this.inicial = inicial;
        this.aceptacion = aceptacion;

    }
    
    
    /**
     * Reinicia lo valores del automata
     **/
    public void resetAutomata(){
        cont = 0;
    }

    /**
     * Manda a llamar el metodo buscarRuta() para ver si la cadena es valida y es valida cuando se encuentra almenos  una ruta
     *
     * @param cadena: Recibe la cadena que va a ser validada
     * @return Si la cadena es acetada devuelve True, de lo contrario False
     * @see Automata#buscarRuta(java.lang.String, int)
     */
    public boolean validarCadena(String cadena) {
        this.cadena = cadena;
        buscarRuta(inicial, 0);//Buscas todas las trayectorias posibles
        return cont > 0;
    }

    /**
     * Recorre cada rama posible del automata, al terminar una rama regresa a
     * los nodos anteriores para buscar otra posible rama
     *
     * @param nodo: Estado actual del recorrido
     * @param index : Indice de la cadena que se esta anilizando
     */
    public void buscarRuta(String nodo, int index) {
        if (index == cadena.length()) {//Si el indice que recibe es igual a la longitud de la cadena, entonces se termina la busqueda
            
            for (String l : aceptacion) { //compara el utimo estado con cada estado de aceptacion
                if (nodo.equals(l)) { //Si coicide la cadena es valida
                    System.out.println("Cadena aceptada con trayectoria:"); //Solo imprime el recorrido si el ultimo elemento de la pila es un estado de aceptacion
                    System.out.print(inicial);
                    for (Transicion t : recorrido) {//Recorre la pila con los diferentes nodos del recorrido
                        System.out.print(t);
                    }
                    System.out.println("\n");
                    cont++;//Cuenta el numero de trayectorias validas encontradas
                }
            }

        }
        for (Transicion t : tabla) {//Busca las diferentes trayectorias posibles recorriendo la tabal de transiciones
            if((t.getCadena()).equals("E") && t.getOrigen().equals(nodo)){//Busca y agrega transicion con epsilon               
                recorrido.push(t);//Se coloca el estado en la pila de los recorridos
                buscarRuta(t.getDestino(), index);//Buscar otra ruta pero no avanza en el indice de la cadena ya es es epsilon
                recorrido.pop();//Se han comparado todas las transiciones posibles por lo tanto se ha conluido la busqueda y se termina la trayectoria
                    //se saca el ultimo elemento en la pila para buscar mas rutas en el nodo anterior
            }
            
            if (index < cadena.length())//Si aun hay caracteres por analizas continua la busqueda
            {
                if ((cadena.charAt(index)) == (t.getCadena().charAt(0)) && t.getOrigen().equals(nodo)) {//Busca coincidencia en la tabla de transiciones, de acuerdo al estado actual y el caracter que se esta analizando
                    recorrido.push(t);//Se coloca el estado en la pila de los recorridos
                    buscarRuta(t.getDestino(), index + 1);//Se busca ahora el siguiente nodo, para el caracter siguiente
                    recorrido.pop();//Se han comparado todas las transiciones posibles por lo tanto se ha conluido la busqueda y se termina la trayectoria
                    //se saca el ultimo elemento en la pila para buscar mas rutas en el nodo anterior
                }
            }
        }

    }
}

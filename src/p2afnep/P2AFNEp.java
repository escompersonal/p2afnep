/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package p2afnep;

import javax.swing.JOptionPane;

/**
 *  Clase principal 
 * @author carlos
 */
public class P2AFNEp {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ControladorArchivo ca = new ControladorArchivo();
        ca.imprimirArchivo();
        String inicial = ca.obtenerInicial();
        String[] aceptacion = ca.obtenerEdosAceptacion();
        TablaTrans tabla = ca.obtenerTabla();

        Automata a = new Automata(tabla, inicial, aceptacion);
        boolean rep = true;
        while (rep) {
            a.resetAutomata();
            System.out.println("-------------------------------------------------------------");
            String cadena = JOptionPane.showInputDialog("Cadena a validar: ");
            if (cadena != null) {
                if (!(a.validarCadena(cadena))) {
                    System.out.println("Cadena no valida");
                }
                System.out.println("-------------------------------------------------------------");

                int ax = JOptionPane.showConfirmDialog(null, "Validar otra cadena?", "Repetir validacion", JOptionPane.YES_NO_OPTION);
                if (!(ax == JOptionPane.YES_OPTION)) {
                    rep = false;
                }

            }else{
                rep = false;
            }

        }
    }

}

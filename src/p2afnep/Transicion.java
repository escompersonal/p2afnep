/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package p2afnep;

/**
 * Almacena informacion sobre una transicio determinada
 * @author carlos
 */
public class Transicion {
    private String origen;
    private String destino;
    private String cadena;
    

    /**
    *
    * @return regresa el nodo origen de una transicion
    */
    public String getOrigen() {
        return origen;
    }
    
    /**
    *
    * @param origen: cadena que representara el origen de una transicion
    */
    public void setOrigen(String origen) {
        this.origen = origen;
    }

    /**
    *
    * @return regresa el destino que tiene un transicion
    */
    public String getDestino() {
        return destino;
    }
    
    
    /**
    *
    * @param destino: Caracter que representa el destino de una transicion;
    */
    public void setDestino(String destino) {
        this.destino = destino;
    }

    /**
    *
    * @return regresa la cadena corespondiente a una transicion
    */
    public String getCadena() {
        return cadena;
    }

    /**
    *
    * @param cadena: cadena que corresponde a la transicion
    */
    public void setCadena(String cadena) {
        this.cadena = cadena;
    }
    
    /**
    * Se sobrecarga el metodo Object.toString() imprimir con print
    * @return Informacion de intteres e el recorrido, que contiene el objeto
    * @see Object#toString() 
    */
    @Override
    public String toString() {
        return "("+cadena+") -> "+destino;
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package p2afnep;

import java.util.List;

/**
 * Realiza ooperacione necesarias sobre la salida obtenida al leer el archivo de entrada
 * @author carlos
 */
public class ControladorArchivo {

    private LectorFichero lf;//clase que lee el archivo de entrada
    private List<String> lineas;

    public ControladorArchivo() {
        lf = new LectorFichero("in");//se le pasa la ruta del archivo de entrada
        lineas = lf.leerArchivo();
    }
    /**
     * Lee la linea 2 de acuerdo con la estructura que se indico del archivo
     * @return Devuelve la cadena que contien el estado inicial
     */
    public String obtenerInicial() {
        return lineas.get(2);
    }
    /**
     * Lee la linea 3 de acuerdo con la estructura que se indico del archivo
     * @return Devuelve un arreglo con un estado de aceptacion en cada localidad
     */
    public String[] obtenerEdosAceptacion() {
        return (lineas.get(3)).split(",");
    }

    /**
     * Lee todas las lineas que contienen las transiciones y genera el Objeto
     * TablaTrans
     *
     * @see TablaTrans
     * @return Devuelve la tabla generada
     */
    public TablaTrans obtenerTabla() {
        TablaTrans tabla = new TablaTrans();
        String[] trans;
        for (int i = 4; i < lineas.size(); i++) { //Inicia a leer desde la linea 4 de acuerdo con la estructura que se indico del archivo
            trans = (lineas.get(i)).split(",");//Sepaar las cadenaas por comas
            Transicion t = new Transicion();//Por cada linea se genera un objeto Transicio y se agrega a la lista
            t.setOrigen(trans[0]);
            t.setCadena(trans[1]);
            t.setDestino(trans[2]);
            tabla.add(t);//Se agrega la Transicion a la lista
        }
        return tabla;
    }

    
    /**
     * Obtiene cada linea de la lista y las imprime (Imprime el archivo)
     */
    public void imprimirArchivo() {
        System.out.println("---------------ARCHIVO-----------------------");
        for (String linea : lineas) {//Para cada String de la lista de lineas:
            System.out.println(linea);
        }
        System.out.println("-----------------FIN-------------------------");
    }

}

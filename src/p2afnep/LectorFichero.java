/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package p2afnep;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Realiza la lectura del archivo  colocando cada linea en una lista de Strings
 *
 * @author carlos
 */
public class LectorFichero {

    private String path;
    private File file;
    
    FileReader fr;
    BufferedReader br;

    /**
     * Recibe la ruta del fichero y abre el flujo de lectura
     *
     * @param path : ruta donde se encuentra el fichero a leer
     */

    public LectorFichero(String path) {
        this.path = path;       

        try {
            file = new File(path);
            fr = new FileReader(file);
            br = new BufferedReader(fr);
        } catch (FileNotFoundException ex) {
            cerrarFichero();
        }

    }
    
    /**
     *
     * @return Devuelve la lista que contiene cada linea leida
     */
    public List<String> leerArchivo() {
        List<String> list = new ArrayList<String>();

        try {
            String linea;
            while ((linea = br.readLine()) != null) {//Recorre todo el archivo hasta el final linea a linea
                list.add(linea);
            }
            cerrarFichero();//Al terminar la lectura cierra el archivo
        } catch (IOException ex) {
            cerrarFichero();
        }
        return list;
    }

    /**
     *
     * cierra el fichero
     */
    public void cerrarFichero() {
        try {
            if (null != fr) {
                fr.close();
            }
        } catch (IOException ex) {

        }
    }

}
